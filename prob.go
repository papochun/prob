package main

import (
  "math"
  "fmt"
  "strconv"
  "log"
)

func main() {
  var buffer string

  // the chance to get the reward in one attempt
  fmt.Printf("Percent Drop Chance: ")
	fmt.Scanln(&buffer)

  if buffer == "" {
    fmt.Printf("Defaulting to '15%%'\n")
    buffer = "15"
  }

  chance, err := strconv.ParseFloat(buffer, 64)
	if err != nil {
		log.Fatal(err)
	}
  buffer = ""

  // turn into decimal (5 -> 0.05)
  // and invert the chance (success -> failure)
  adjusted := 1 - (chance / 100)

  // the desired chance to get the reward
  fmt.Printf("Desired Percent Chance: ")
	fmt.Scanln(&buffer)

  if buffer == "" {
    fmt.Printf("Defaulting to '75%%'\n")
    buffer = "75"
  }

  desired, err := strconv.ParseFloat(buffer, 64)
	if err != nil {
		log.Fatal(err)
	}
  buffer = ""

  expected := attemptsRequired(adjusted, 50)
  very_likely := attemptsRequired(adjusted, 90)
  nearly_guaranteed := attemptsRequired(adjusted, 99.9)
  required := attemptsRequired(adjusted, desired)

  fmt.Printf("%.f attempts: Expected (50%%)\n", expected)
  fmt.Printf("%.f attempts: Very Likely (90%%)\n", very_likely)
  fmt.Printf("%.f attempts: Nearly Guaranteed (99.9%%)\n", nearly_guaranteed)
  fmt.Printf("%.f attempts: Goal (%.f%%)\n", required, desired)
}

// uses a loop to determine the required attempts to reach a desired probability
func attemptsRequired(chance_fail float64, success float64) (float64) {
	var total_chance float64
	var count float64
	for count = float64(0); total_chance <= success; count++ {
		total_chance = 0
		total_chance = probability(chance_fail, count)
	}
	return count
}

// determines the probability given a desired amount of attempts
func probability(chance_fail float64, count float64) (float64) {
	failure := math.Pow(chance_fail, count)
	success := float64(1) - failure
	return float64(100) * success
}
